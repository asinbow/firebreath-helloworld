﻿/**********************************************************\

  Auto-generated HelloWorldAPI.cpp

\**********************************************************/

#include "JSObject.h"
#include "variant_list.h"
#include "DOM/Document.h"
#include "global/config.h"

#include "HelloWorldAPI.h"
#include "stdlib.h"

///////////////////////////////////////////////////////////////////////////////
/// @fn FB::variant HelloWorldAPI::echo(const FB::variant& msg)
///
/// @brief  Echos whatever is passed from Javascript.
///         Go ahead and change it. See what happens!
///////////////////////////////////////////////////////////////////////////////
FB::variant HelloWorldAPI::echo(const FB::variant& msg)
{
    static int n(0);
    fire_echo("So far, you clicked this many times: ", n++);

    // return "foobar";
    return msg;
}

///////////////////////////////////////////////////////////////////////////////
/// @fn HelloWorldPtr HelloWorldAPI::getPlugin()
///
/// @brief  Gets a reference to the plugin that was passed in when the object
///         was created.  If the plugin has already been released then this
///         will throw a FB::script_error that will be translated into a
///         javascript exception in the page.
///////////////////////////////////////////////////////////////////////////////
HelloWorldPtr HelloWorldAPI::getPlugin()
{
    HelloWorldPtr plugin(m_plugin.lock());
    if (!plugin) {
        throw FB::script_error("The plugin is invalid");
    }
    return plugin;
}

// Read/Write property testString
std::string HelloWorldAPI::get_testString()
{
    return m_testString;
}

void HelloWorldAPI::set_testString(const std::string& val)
{
    m_testString = val;
}

// Read-only property version
std::string HelloWorldAPI::get_version()
{
    return FBSTRING_PLUGIN_VERSION;
}

void HelloWorldAPI::testEvent()
{
    fire_test();
}

int HelloWorldAPI::add(int a, int b)
{
	return a + b;
}

int HelloWorldAPI::exec(const std::string& path)
{
	return system(path.c_str());
}

int HelloWorldAPI::accumulate(const std::vector<int>& values)
{
	return std::accumulate(values.begin(), values.end(), 0);
}

int HelloWorldAPI::count(const std::map<std::string, std::string>& obj)
{
	return obj.size();
}

void HelloWorldAPI::each_char(const std::string& str, const FB::JSObjectPtr& callback)
{
	for (std::string::const_iterator ite = str.begin(); ite != str.end(); ite++)
	{
		callback->InvokeAsync("", FB::variant_list_of(*ite));
	}
}

void HelloWorldAPI::set_name(const std::string& name)
{
	if (name != m_name)
	{
		fire_rename(m_name, name);
	}
	m_name = name;
}

std::string HelloWorldAPI::get_name()
{
	return m_name;
}
