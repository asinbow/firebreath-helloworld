﻿/**********************************************************\

  Auto-generated HelloWorldAPI.h

\**********************************************************/

#include <string>
#include <sstream>
#include <boost/weak_ptr.hpp>
#include "JSAPIAuto.h"
#include "BrowserHost.h"
#include "HelloWorld.h"
#include <vector>
#include <map>
#include <numeric>

#ifndef H_HelloWorldAPI
#define H_HelloWorldAPI

class HelloWorldAPI : public FB::JSAPIAuto
{
public:
    ////////////////////////////////////////////////////////////////////////////
    /// @fn HelloWorldAPI::HelloWorldAPI(const HelloWorldPtr& plugin, const FB::BrowserHostPtr host)
    ///
    /// @brief  Constructor for your JSAPI object.
    ///         You should register your methods, properties, and events
    ///         that should be accessible to Javascript from here.
    ///
    /// @see FB::JSAPIAuto::registerMethod
    /// @see FB::JSAPIAuto::registerProperty
    /// @see FB::JSAPIAuto::registerEvent
    ////////////////////////////////////////////////////////////////////////////
    HelloWorldAPI(const HelloWorldPtr& plugin, const FB::BrowserHostPtr& host) :
        m_plugin(plugin), m_host(host)
    {
        registerMethod("echo",       make_method(this, &HelloWorldAPI::echo));
        registerMethod("exec",       make_method(this, &HelloWorldAPI::exec));
        registerMethod("add",        make_method(this, &HelloWorldAPI::add));
        registerMethod("accumulate", make_method(this, &HelloWorldAPI::accumulate));
        registerMethod("count",      make_method(this, &HelloWorldAPI::count));
        registerMethod("each_char",  make_method(this, &HelloWorldAPI::each_char));
        registerMethod("testEvent",  make_method(this, &HelloWorldAPI::testEvent));
        
        // Read-write property
        registerProperty("testString",
                         make_property(this,
                                       &HelloWorldAPI::get_testString,
                                       &HelloWorldAPI::set_testString));
        
        // Read-only property
        registerProperty("version",
                         make_property(this,
                                       &HelloWorldAPI::get_version));

        registerProperty("name",
                         make_property(this,
                                       &HelloWorldAPI::get_name,
                                       &HelloWorldAPI::set_name
									   ));
		registerAttribute("id", "007");

    }

    ///////////////////////////////////////////////////////////////////////////////
    /// @fn HelloWorldAPI::~HelloWorldAPI()
    ///
    /// @brief  Destructor.  Remember that this object will not be released until
    ///         the browser is done with it; this will almost definitely be after
    ///         the plugin is released.
    ///////////////////////////////////////////////////////////////////////////////
    virtual ~HelloWorldAPI() {};

    HelloWorldPtr getPlugin();

    // Read/Write property ${PROPERTY.ident}
    std::string get_testString();
    void set_testString(const std::string& val);

    // Read-only property ${PROPERTY.ident}
    std::string get_version();

    // Method echo
    FB::variant echo(const FB::variant& msg);
	int exec(const std::string& path);
	int add(int a, int b);
	int accumulate(const std::vector<int>& values);
	int count(const std::map<std::string, std::string>& obj);
	void each_char(const std::string& str, const FB::JSObjectPtr& callback);
    
    // Event helpers
    FB_JSAPI_EVENT(test, 0, ());
    FB_JSAPI_EVENT(echo, 2, (const FB::variant&, const int));
	FB_JSAPI_EVENT(rename, 2, (const std::string&, const std::string&));

    // Method test-event
    void testEvent();

private:
    HelloWorldWeakPtr m_plugin;
    FB::BrowserHostPtr m_host;

    std::string m_testString;

public:
    void set_name(const std::string& name);
    std::string get_name();
protected:
    std::string m_name;

};

#endif // H_HelloWorldAPI

