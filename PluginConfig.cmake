#/**********************************************************\ 
#
# Auto-Generated Plugin Configuration file
# for HelloWorld
#
#\**********************************************************/

set(PLUGIN_NAME "HelloWorld")
set(PLUGIN_PREFIX "HWO")
set(COMPANY_NAME "HAO")

# ActiveX constants:
set(FBTYPELIB_NAME HelloWorldLib)
set(FBTYPELIB_DESC "HelloWorld 1.0 Type Library")
set(IFBControl_DESC "HelloWorld Control Interface")
set(FBControl_DESC "HelloWorld Control Class")
set(IFBComJavascriptObject_DESC "HelloWorld IComJavascriptObject Interface")
set(FBComJavascriptObject_DESC "HelloWorld ComJavascriptObject Class")
set(IFBComEventSource_DESC "HelloWorld IFBComEventSource Interface")
set(AXVERSION_NUM "1")

# NOTE: THESE GUIDS *MUST* BE UNIQUE TO YOUR PLUGIN/ACTIVEX CONTROL!  YES, ALL OF THEM!
set(FBTYPELIB_GUID d6b63a67-c14a-5472-ab1b-ca1eb56da695)
set(IFBControl_GUID af6c59fc-b10d-55af-a0d5-a3483d61de8d)
set(FBControl_GUID 431e0ccb-9f81-56d6-9338-63b1d3222f2d)
set(IFBComJavascriptObject_GUID 4caadbde-5772-55fa-a421-dced6dc58cc9)
set(FBComJavascriptObject_GUID 4d6721f9-4b67-5012-bf0e-53fda96574e8)
set(IFBComEventSource_GUID 2e7d9ed6-7a0e-5459-9f1d-577076454c80)
if ( FB_PLATFORM_ARCH_32 )
    set(FBControl_WixUpgradeCode_GUID 47192da4-df2e-58a5-966f-0fb6c60bc178)
else ( FB_PLATFORM_ARCH_32 )
    set(FBControl_WixUpgradeCode_GUID f741b560-db20-5005-8cf9-3b7e4d30c4a2)
endif ( FB_PLATFORM_ARCH_32 )

# these are the pieces that are relevant to using it from Javascript
set(ACTIVEX_PROGID "HAO.HelloWorld")
if ( FB_PLATFORM_ARCH_32 )
    set(MOZILLA_PLUGINID "hao.com/HelloWorld")  # No 32bit postfix to maintain backward compatability.
else ( FB_PLATFORM_ARCH_32 )
    set(MOZILLA_PLUGINID "hao.com/HelloWorld_${FB_PLATFORM_ARCH_NAME}")
endif ( FB_PLATFORM_ARCH_32 )

# strings
set(FBSTRING_CompanyName "HAO")
set(FBSTRING_PluginDescription "by hao")
set(FBSTRING_PLUGIN_VERSION "1.0.0.0")
set(FBSTRING_LegalCopyright "Copyright 2014 HAO")
set(FBSTRING_PluginFileName "np${PLUGIN_NAME}")
set(FBSTRING_ProductName "HelloWorld")
set(FBSTRING_FileExtents "")
if ( FB_PLATFORM_ARCH_32 )
    set(FBSTRING_PluginName "HelloWorld")  # No 32bit postfix to maintain backward compatability.
else ( FB_PLATFORM_ARCH_32 )
    set(FBSTRING_PluginName "HelloWorld_${FB_PLATFORM_ARCH_NAME}")
endif ( FB_PLATFORM_ARCH_32 )
set(FBSTRING_MIMEType "application/x-helloworld")

# Uncomment this next line if you're not planning on your plugin doing
# any drawing:

#set (FB_GUI_DISABLED 1)

# Mac plugin settings. If your plugin does not draw, set these all to 0
set(FBMAC_USE_QUICKDRAW 0)
set(FBMAC_USE_CARBON 1)
set(FBMAC_USE_COCOA 1)
set(FBMAC_USE_COREGRAPHICS 1)
set(FBMAC_USE_COREANIMATION 0)
set(FBMAC_USE_INVALIDATINGCOREANIMATION 0)

# If you want to register per-machine on Windows, uncomment this line
#set (FB_ATLREG_MACHINEWIDE 1)
